﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance { get; set; }

    private Dictionary<int, Vector2> activeTouches = new Dictionary<int, Vector2>();

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public Vector3 GetPlayerInput()
    {
        // Are we using accelerometer?
        if (SaveManager.Instance.state.usingAccelerometer)
        {
            // If we can use it, replace Y by Z.
            Vector3 temp;

            // Acceleration fix number.
            temp.x = Input.acceleration.x * 135;
            temp.y = 0f;
            temp.z = 0f;

            return temp;
        }

        //Read all touches from the user.

        Vector3 t = Vector3.zero;
        foreach(Touch touch in Input.touches)
        {
            // Are we touching?
            if(touch.phase == TouchPhase.Began)
            {
                activeTouches.Add(touch.fingerId, touch.position);
            }
            // Are we ended touching?
            else if(touch.phase == TouchPhase.Ended)
            {
                if (activeTouches.ContainsKey(touch.fingerId))
                { 
                    activeTouches.Remove(touch.fingerId);
                }
            }
            // Moving or stationary
            else
            {
                float magnitude = 0;
                t = (touch.position - activeTouches[touch.fingerId]);
                magnitude = t.magnitude / 10;
                t = t.normalized * magnitude;
            }
        }

        return t;
    }
}
