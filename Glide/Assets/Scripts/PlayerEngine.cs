﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerEngine : MonoBehaviour
{
    private CharacterController controller;

    private float playerSpeed = 10.0f;
    private float rotSpeedX = .1f;
    private float rotSpeedY = .05f;

    private float deathTime;
    private float deathDuration = 2f;

    public GameObject explosionFX;

    private void Start()
    {
        controller = GetComponent<CharacterController>();

        // Create the trail
        GameObject trail = Instantiate(ShopManager.Instance.playerTrails[SaveManager.Instance.state.activeTrail]) as GameObject;

        // Set trail as child of the model
        trail.transform.SetParent(transform.GetChild(0));

        // Fix position
        trail.transform.localPosition = new Vector3(-2.5f, 0, 0);
        trail.transform.localEulerAngles = Vector3.zero;
    }

    private void Update()
    {
        // Player dies
        if(deathTime != 0)
        {
            if(Time.time - deathTime > deathDuration)
            {
                SceneManager.LoadScene("Game");
            }
        }


        // Player velocity
        Vector3 moveVector = transform.forward * playerSpeed;

        // Player input
        Vector3 inputs = PlayerManager.Instance.GetPlayerInput();

        Vector3 yaw = inputs.x * transform.right * rotSpeedX * Time.deltaTime;
        Vector3 pitch = inputs.y * transform.up * rotSpeedY * Time.deltaTime;

        Vector3 dir = yaw + pitch;

        float maxX = Quaternion.LookRotation(moveVector + dir).eulerAngles.x;

        if(maxX < 90 && maxX > 70 || maxX > 270 && maxX < 290)
        {
            // Too far
        }
        else
        {
            // Add direction to the current move
            moveVector += dir;

            // Have player face where is going
            transform.rotation = Quaternion.LookRotation(moveVector);
        }

        // Move!
        controller.Move(moveVector * Time.deltaTime);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        deathTime = Time.time;

        GameObject explosion = Instantiate(explosionFX) as GameObject;
        explosion.transform.position = transform.position;

        // Hide player
        transform.GetChild(0).gameObject.SetActive(false);

    }

    // Delete this later!!
    public void OnNitro()
    {
        if (playerSpeed == 10.0f)
            playerSpeed = 50.0f;
        else
            playerSpeed = 10.0f;
    }
}
