﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour
{
    private List<Transform> rings = new List<Transform>();

    private int ringsPassed = 0;

    public Material activeRing;
    public Material inactiveRing;
    public Material finalRing;

    private void Start()
    {
        // Set the objective variable in the GameScene script
        FindObjectOfType<GameScene>().objective = this;

        // Set all the rings are inactive.
        foreach (Transform t in transform)
        {
            rings.Add(t);
            t.GetComponent<MeshRenderer>().material = inactiveRing;
        }

        if(rings.Count == 0)
        {
            Debug.Log("There is no ring in the scene!");
            return;
        }

        // Activate first ring.
        rings[ringsPassed].GetComponent<MeshRenderer>().material = activeRing;
        rings[ringsPassed].GetComponent<Ring>().ActivateRing();
    }

    public Transform GetCurrentRing()
    {
        return rings[ringsPassed];
    }
    public void NextRing()
    {
        // Put an effect

        ringsPassed++;

        // If it is final ring, you win!
        if (ringsPassed == rings.Count)
        {
            Victory();
        }

        if(ringsPassed == rings.Count - 1)
        {
            rings[ringsPassed].GetComponent<MeshRenderer>().material = finalRing;
        }
        else
        {
            rings[ringsPassed].GetComponent<MeshRenderer>().material = activeRing;
        }

        rings[ringsPassed].GetComponent<Ring>().ActivateRing();
    }

    private void Victory()
    {
        FindObjectOfType<GameScene>().CompleteLevel();
    }

}
