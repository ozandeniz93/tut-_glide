﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { get; set; }
    public SaveState state;

    private void Awake()
    {
        //ResetSaveFile();
        DontDestroyOnLoad(gameObject);
        Instance = this;
        Load();


        // Using accelerometer AND can we use it?
        if(state.usingAccelerometer && !SystemInfo.supportsAccelerometer)
        {
            // If we can't make sure we don't try it again.
            state.usingAccelerometer = false;
            Save();
        }
    }

    public void Save()
    {
         PlayerPrefs.SetString("save", Helper.Encrypt(Helper.Serialize<SaveState>(state)));
    }
    public void Load()
    {
        if (PlayerPrefs.HasKey("save"))
        {
            state = Helper.Deserialize<SaveState>(Helper.Decrypt(PlayerPrefs.GetString("save")));
        }

        else
        {
            Debug.Log("No save file found!");

            // New save file.
            state = new SaveState();
            Save();


        }
    }

    public bool IsColorOwned(int index)
    {
        // If the bit number at index's left(+1) in colorOwned integer is not equal to 0, the color owned.
        return (state.colorOwned & (1 << index)) != 0;
    }
    public bool IsTrailOwned(int index)
    {
        // If the bit number at index's left(+1) in trailOwned integer is not equal to 0, the trail owned.
        return (state.trailOwned & (1 << index)) != 0;
    }

    // ^= -> close the bit
    // |= -> open the bit
    public void UnlockColor(int index)
    {
        state.colorOwned |= 1 << index; // Toggle on the bit at index's left(+1) .
    }
    public void UnlockTrail(int index)
    {
        state.trailOwned |= 1 << index; // Toggle on the bit at index's left(+1).
    }

    public void CompleteLevel(int index)
    {
        // If this is the active level
        if (state.levelCompleted == index)
        {
            state.levelCompleted++;
            Save();
        }
    }

    public void ResetSaveFile()
    {
        PlayerPrefs.DeleteKey("save");
    }

    //Attempt to buy a color
    public bool BuyColor(int index, int cost)
    {
        if (state.gold >= cost)
        {
            // We have enough gold, substract the gold.
            state.gold -= cost;
            UnlockColor(index);

            Save();
            return true;
        }
        else
        {
            return false;
        }

    }

    //Attempt to buy a trail
    public bool BuyTrail(int index, int cost)
    {
        if (state.gold >= cost)
        {
            // We have enough gold, substract the gold.
            state.gold -= cost;
            UnlockTrail(index);

            Save();
            return true;
        }
        else
        {
            return false;
        }

    }
}
