﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScene : MonoBehaviour
{
    private CanvasGroup fadeGroup;
    private float fadeInDuration = 2f;
    private bool gameStarted;

    public Transform arrow;
    public Objective objective;
    private Transform playerTransform;

    private void Start()
    {

        // Find player
        playerTransform = GameObject.Find("Player").transform;


        // Load the proper level for the Game Scene
        SceneManager.LoadScene(LevelManager.Instance.currentLevel.ToString(), LoadSceneMode.Additive);


        fadeGroup = FindObjectOfType<CanvasGroup>();
        fadeGroup.alpha = 1;
    }

    private void Update()
    {
        if(objective != null)
        {
            // Rotate the arrow
            Vector3 direction = playerTransform.InverseTransformPoint(objective.GetCurrentRing().position);
            float x = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            x += 180;

            // y is 180 because arrow image is downward
            arrow.transform.localEulerAngles = new Vector3(0, 180, x);
        }

        if(Time.timeSinceLevelLoad <= fadeInDuration)
        {
            fadeGroup.alpha = 1 - (Time.timeSinceLevelLoad / fadeInDuration);
        }
        else if (!gameStarted)
        {
            fadeGroup.alpha = 0;
            gameStarted = true;
        }
    }

    public void CompleteLevel()
    {
        // Complete the level and save the progress.
        SaveManager.Instance.CompleteLevel(LevelManager.Instance.currentLevel);

        // Go back to level selection menu
        LevelManager.Instance.menuFocus = 1;

        ExitLevel();
    }
    public void ExitLevel()
    {
        SceneManager.LoadScene("Menu");
    }
	
}
