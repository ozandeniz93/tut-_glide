﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScene : MonoBehaviour
{
    private MenuCamera menuCam;

    private CanvasGroup fadeGroup;
    private float fadeInSpeed = 0.33f;

    public RectTransform menuContainer;

    public Transform levelPanel;
    public Transform colorPanel;
    public Transform trailPanel;

    public Text colorBuySet;
    public Text trailBuySet;
    public Text goldText;

    private int[] colorCosts = new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45 };
    private int[] trailCosts = new int[] { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450 };

    private int selectedColorIndex;
    private int selectedTrailIndex;

    private int activeColorIndex;
    private int activeTrailIndex;
    
    private Vector3 desiredMenuPosition;

    public AnimationCurve enteringLevelZoomCurve;
    private bool isEnteringLevel = false;
    private float zoomDuration = 3.0f;
    private float zoomTransition;

    private Texture previousTexture;
    private GameObject lastPreviewObject;
    public Transform trailPreviewObject;
    public RenderTexture trailPreviewTexture;

    private GameObject currentTrail;

    public Button tiltControlButton;
    public Color tiltEnabled;
    public Color tiltDisabled;


    private void Start()
    {
        SaveManager.Instance.state.gold = 99999;

        // Check if we have accelerometer
        if (SystemInfo.supportsAccelerometer)
        {
            tiltControlButton.GetComponent<Image>().color = (SaveManager.Instance.state.usingAccelerometer)
                ? tiltEnabled
                : tiltDisabled;
        }
        else
        {
            tiltControlButton.gameObject.SetActive(false);
        }


        menuCam = FindObjectOfType<MenuCamera>();

        SetCameraTo(LevelManager.Instance.menuFocus);
        // How much gold we have?
        UpdateGoldText();

        // Get the only canvasGroup on scene.
        fadeGroup = FindObjectOfType<CanvasGroup>();

        // Set screen white.
        fadeGroup.alpha = 1;

        // Add button on-click events to shop buttons.
        InitializeShop();

        // Add button on-click events to level buttons.
        InitializeLevel();

        // Set player preferences ( color and trail selection)
        OnColorSelect(SaveManager.Instance.state.activeColor);
        SetColor(SaveManager.Instance.state.activeColor);

        OnTrailSelect(SaveManager.Instance.state.activeTrail);
        SetTrail(SaveManager.Instance.state.activeTrail);

        // Make the buttons bigger
        colorPanel.GetChild(SaveManager.Instance.state.activeColor).GetComponent<RectTransform>().localScale = Vector3.one * 1.125f;
        trailPanel.GetChild(SaveManager.Instance.state.activeTrail).GetComponent<RectTransform>().localScale = Vector3.one * 1.125f;

        // Create trail preview
        lastPreviewObject = GameObject.Instantiate(ShopManager.Instance.playerTrails[SaveManager.Instance.state.activeTrail]) as GameObject;
        lastPreviewObject.transform.SetParent(trailPreviewObject);
        lastPreviewObject.transform.localPosition = Vector3.zero;


    }

    private void Update()
    {
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;

        // Menu navigation
        menuContainer.anchoredPosition3D = Vector3.Lerp(menuContainer.anchoredPosition3D, desiredMenuPosition, 0.1f);

        // Entering level zoom
        if (isEnteringLevel)
        {
            zoomTransition += (1 / zoomDuration) * Time.deltaTime;

            // Change the scale according to a curve.
            menuContainer.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 5, enteringLevelZoomCurve.Evaluate(zoomTransition));

            // Zoom into the clicked position (clicked level in this case)
            Vector3 newDesiredPosition = desiredMenuPosition * 5;

            // Specific position of the level on canvas
            RectTransform rt = levelPanel.GetChild(LevelManager.Instance.currentLevel).GetComponent<RectTransform>();
            newDesiredPosition -= rt.anchoredPosition3D * 5;

            menuContainer.anchoredPosition3D = Vector3.Lerp(desiredMenuPosition, newDesiredPosition, enteringLevelZoomCurve.Evaluate(zoomTransition));

            fadeGroup.alpha = zoomTransition;

            // Is the animation over
            if(zoomTransition >= 1)
            {
                SceneManager.LoadScene("Game");
            }
        }
    }


    private void InitializeShop()
    {
        if (colorPanel == null || trailPanel == null)
        {
            Debug.Log("Assign the trail/color panels!");
        }

        int i = 0;
        foreach (Transform t in colorPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnColorSelect(currentIndex));

            // If the color is owned or not, change the color shade depending on.
            Image img = t.GetComponent<Image>();
            img.color = SaveManager.Instance.IsColorOwned(currentIndex)
                ?
                ShopManager.Instance.playerColors[currentIndex] 
                : 
                Color.Lerp(ShopManager.Instance.playerColors[currentIndex],new Color(0,0,0, 1),.25f);

            i++;
        }

        i = 0;
        foreach (Transform t in trailPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnTrailSelect(currentIndex));

            // If the trail is owned or not, change the color shade depending on.
            RawImage img = t.GetComponent<RawImage>();
            img.color = SaveManager.Instance.IsTrailOwned(currentIndex)
                ?
                Color.white
                :
                new Color(.7f, .7f, .7f);

            i++;
        }

        // Set previous trail, before the trail texture swap.
        previousTexture = trailPanel.GetChild(SaveManager.Instance.state.activeTrail).GetComponent<RawImage>().texture;
    }
    private void InitializeLevel()
    {
        if (levelPanel == null)
        {
            Debug.Log("Assign the level panel!");
        }

        int i = 0;
        foreach (Transform t in levelPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnLevelSelect(currentIndex));

            Image img = t.GetComponent<Image>();

            if (currentIndex <= SaveManager.Instance.state.levelCompleted)
            {
                // Level is unlocked!
                if (currentIndex == SaveManager.Instance.state.levelCompleted)
                {
                    // Level is not completed!
                    img.color = Color.white;
                }
                else
                {
                    //Level is completed!
                    img.color = Color.green;
                }

            }
            else
            {
                // Level is not unlocked!
                b.interactable = false;
                img.color = Color.grey;
            }

            i++;
        }
    }

    private void OnColorSelect(int currentIndex)
    {
        Debug.Log("Selected color: " + currentIndex);

        // If the clicked button is already selected, do nothing.
        if (selectedColorIndex == currentIndex)
            return;

        // Make sure selected one is a little bit bigger 
        // and the other (before) selected one is going back to normal size
        colorPanel.GetChild(currentIndex).GetComponent<RectTransform>().localScale = Vector3.one * 1.125f;
        colorPanel.GetChild(selectedColorIndex).GetComponent<RectTransform>().localScale = Vector3.one;





        selectedColorIndex = currentIndex;

        if (SaveManager.Instance.IsColorOwned(currentIndex))
        {
            // Color is owned 
            if (activeColorIndex == currentIndex)
            {
                colorBuySet.text = "Activate!";
            }

            else
            { 
                colorBuySet.text = "Select";
            }
        }
        else
        {
            // Color is not owned
            colorBuySet.text = "Buy: $" + colorCosts[currentIndex].ToString();
        }

    }
    private void OnTrailSelect(int currentIndex)
    {
        Debug.Log("Selected trail: " + currentIndex);

        // If the clicked button is already selected, do nothing.
        if (selectedTrailIndex == currentIndex)
            return;

        // Preview trail
        trailPanel.GetChild(selectedTrailIndex).GetComponent<RawImage>().texture = previousTexture;
        previousTexture = trailPanel.GetChild(currentIndex).GetComponent<RawImage>().texture;
        trailPanel.GetChild(currentIndex).GetComponent<RawImage>().texture = trailPreviewTexture;

        if(lastPreviewObject != null)
        {
            Destroy(lastPreviewObject);
        }

        // Create trail preview
        lastPreviewObject = GameObject.Instantiate(ShopManager.Instance.playerTrails[currentIndex]) as GameObject;
        lastPreviewObject.transform.SetParent(trailPreviewObject);
        lastPreviewObject.transform.localPosition = Vector3.zero;
        lastPreviewObject.transform.localRotation = Quaternion.Euler(0, 90, 0);
        


        // Make sure selected one is a little bit bigger 
        // and the other (before) selected one is going back to normal size
        trailPanel.GetChild(currentIndex).GetComponent<RectTransform>().localScale = Vector3.one * 1.125f;
        trailPanel.GetChild(selectedTrailIndex).GetComponent<RectTransform>().localScale = Vector3.one;


        selectedTrailIndex = currentIndex;

        if (SaveManager.Instance.IsTrailOwned(currentIndex))
        {
            // Trail is owned 
            if (activeTrailIndex == currentIndex)
            {
                trailBuySet.text = "Activate!";
            }

            else
            {
                trailBuySet.text = "Select";
            }
        }
        else
        {
            // Trail is not owned
            trailBuySet.text = "Buy: $" + trailCosts[currentIndex].ToString();
        }
    }
    private void OnLevelSelect(int currentIndex)
    {
        Debug.Log("Selected level: " + currentIndex);

        LevelManager.Instance.currentLevel = currentIndex;
        isEnteringLevel = true;
    }

    public void OnColorBuy()
    {
        Debug.Log("Color Buy!");

        if (SaveManager.Instance.IsColorOwned(selectedColorIndex))
        {
            SetColor(selectedColorIndex);
        }
        else
        {
            if (SaveManager.Instance.BuyColor(selectedColorIndex, colorCosts[selectedColorIndex]))
            {
                // Success.Set color immadetly.
                SetColor(selectedColorIndex);

                // Change the button color if it is owned.
                colorPanel.GetChild(selectedColorIndex).GetComponent<Image>().color = ShopManager.Instance.playerColors[selectedColorIndex];

                // Visual update.
                UpdateGoldText();
            }

            else
            {
                // Not enough gold.
                Debug.Log("Not enough gold!");
            }
        }
    }
    public void OnTrailBuy()
    {
        Debug.Log("Trail Buy!");
        if (SaveManager.Instance.IsTrailOwned(selectedTrailIndex))
        {
            SetTrail(selectedTrailIndex);
        }
        else
        {
            if (SaveManager.Instance.BuyTrail(selectedTrailIndex, trailCosts[selectedTrailIndex]))
            {
                // Success. Set trail immediately.
                SetTrail(selectedTrailIndex);

                // Change the button color if it is owned.
                trailPanel.GetChild(selectedTrailIndex).GetComponent<RawImage>().color = Color.white;

                // Visual update.
                UpdateGoldText();
            }

            else
            {
                // Not enough gold.
                Debug.Log("Not enough gold!");
            }
        }
    }

    private void SetColor(int index)
    {
        // Set the active index
        activeColorIndex = index;

        // To load saved selected color
        SaveManager.Instance.state.activeColor = index;

        // Change the color on player model
        ShopManager.Instance.playerMaterial.color = ShopManager.Instance.playerColors[index];

        // Change the buy/set text
        colorBuySet.text = "Current";

        // Save the selected color.
        SaveManager.Instance.Save();
    }
    private void SetTrail(int index)
    {
        // Set the active index
        activeTrailIndex = index;

        // To load saved selected trail
        SaveManager.Instance.state.activeTrail = index;

        // Change the trail on player model
        if (currentTrail != null)
            Destroy(currentTrail);

        currentTrail = Instantiate(ShopManager.Instance.playerTrails[index]) as GameObject;
        currentTrail.transform.SetParent(GameObject.Find("Player").transform);

        currentTrail.transform.localPosition = new Vector3(-2.5f, 0, 0);
        currentTrail.transform.localRotation = Quaternion.identity;
        


        // Change the buy/set text
        trailBuySet.text = "Current";

        // Save the selected trail.
        SaveManager.Instance.Save();
    }

    private void SetCameraTo(int menuIndex)
    {
        NavigateTo(menuIndex);
        menuContainer.anchoredPosition3D = desiredMenuPosition;
    }
    private void NavigateTo(int menuIndex)
    {
        switch (menuIndex)
        {
            // Main Menu
            default:
            case 0: desiredMenuPosition = Vector3.zero;
                menuCam.BackToMainMenu();
                break;

            // Play Menu
            case 1: desiredMenuPosition = Vector3.right * 1280;
                menuCam.MoveToLevel();
                break;
            
            // Shop Menu
            case 2: desiredMenuPosition = Vector3.left * 1280;
                menuCam.MoveToShop();
                break;
        }
    }

    public void OnPlayClick() { NavigateTo(1); Debug.Log("Play clicked"); }
    public void OnShopClick() { NavigateTo(2); Debug.Log("Shop clicked"); }
    public void OnBackClick() { NavigateTo(0); Debug.Log("Back clicked"); }

    private void UpdateGoldText()
    {
        goldText.text = SaveManager.Instance.state.gold.ToString();
    }

    public void OnTiltControlButton()
    {
        SaveManager.Instance.state.usingAccelerometer = !SaveManager.Instance.state.usingAccelerometer;
        SaveManager.Instance.Save();

        // Change the color
        tiltControlButton.GetComponent<Image>().color = (SaveManager.Instance.state.usingAccelerometer)
                ? tiltEnabled
                : tiltDisabled;
    }
}
