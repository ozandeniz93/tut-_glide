﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayer : MonoBehaviour {

    public float speed = 3;

	void Update ()
    {
        transform.localPosition += Vector3.forward * speed * Time.deltaTime;	
	}
}
