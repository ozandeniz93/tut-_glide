﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCamera : MonoBehaviour {

    private Vector3 startPosition;
    private Quaternion startRotation;

    private Vector3 desiredPosition;
    private Quaternion desiredRotation;

    public Transform shopWaypoint;
    public Transform levelWaypoint;

    public Text x;
    public Text y;
    public Text z;

    private void Start()
    {
        startPosition = desiredPosition = transform.localPosition;
        startRotation = desiredRotation = transform.localRotation;
    }

    private void Update()
    {
        x.text = PlayerManager.Instance.GetPlayerInput().x.ToString();
        y.text = PlayerManager.Instance.GetPlayerInput().y.ToString();
        z.text = PlayerManager.Instance.GetPlayerInput().z.ToString();

        float inputx = PlayerManager.Instance.GetPlayerInput().x;

        transform.localPosition = Vector3.Lerp(transform.localPosition, desiredPosition + new Vector3(0, 0, inputx) * .05f, .1f);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, desiredRotation, .1f);
    }

    public void BackToMainMenu()
    {
        desiredPosition = startPosition;
        desiredRotation = startRotation;
    }

    public void MoveToShop()
    {
        desiredPosition = shopWaypoint.localPosition;
        desiredRotation = shopWaypoint.localRotation;
    }

    public void MoveToLevel()
    {
        desiredPosition = levelWaypoint.localPosition;
        desiredRotation = levelWaypoint.localRotation;
    }
}
