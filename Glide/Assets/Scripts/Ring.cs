﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour
{
    private Objective objective;
    private bool ringActive = false;

    private void Start()
    {
        objective = FindObjectOfType<Objective>();
    }

    public void ActivateRing()
    {
        ringActive = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        // If the ring is active, tell to the objective script it's been passed through.
        if (ringActive)
        {
            objective.NextRing();
            Destroy(gameObject, 5.0f);
        }
    }

	
}
